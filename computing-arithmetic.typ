#import "lib.typ": *

= Computing Arithmetic
== Kodierungen
#definition[BCD-Kodierung][
  Die Zahl wird sozusagen zur Basis 10 dargestellt.

  Jede Dezimalziffer wird zu entsprechenden 4 Binärziffern, die aneinandergereiht abgespeichert werden

  - *Vorteil*: Konvertierung in Dezimalzahl einfach
  - *Nachteil*: Suboptimale Speicherplatzausnutzung & Probleme bei der Ausührung arithmetischer Operationen
]

#align(center, image("res/general-float.png", height: 25%))

#image("res/float-normalized.png")
#separate()
#image("res/float-IEEE.png", width: 60%)
