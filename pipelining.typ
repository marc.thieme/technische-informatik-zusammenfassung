#import "lib.typ" as lib: *

= Pipelining
#term[Phasen der Befehlsverarbeitung][
+ Instruction Fetch (*IF*)
+ Instruction Decode (*ID*)
+ Instrcution Execute (*IE*)
+ Memory Access (*MA*)
+ Write Back (*WB*)
]

#term[Befehlszeitbegriffe][
/ Latenzzeit: Befehl von Start eines Befehls bis Ende der Ausführung eines Befehls
/ Zykluszeit: Inverse der Taktfrequenz (Inverse der Instructinen pro Sekunde)
*Ohne Pipelining:*
/ Latenzzeit: Summe der Ausführstufen
/ Zykluszeit: Summe der Ausführstufen
*Mit Pipelining*:
/ Latenzzeit: \# Pipelinestufen $dot$ längste Pipelinestufe
/ Zykluszeit: Länge pro einer Pipelinestufe
]

#theorem[Speedup][
Bei $n$ Befehlen und $k$ Verarbeitungseinheiten.
$
  "Speedup" = (n k) / (n + (k - 1))
$]

#term[Pipelining Konflikte][
  Situationen, in denen die nächste Instruktion nicht im Folgenden Taktzyklus weiter verarbeitet werden kann.
  / Strukturkonflik/Resourcenkonflikt: Selbe Hardwarekonmponente wird konkurrierend von 2 Instruktionen benötigt.
  / Datenkonflikt: 
    - Echte Datenabhängigkeit $->$ Read-After-Write (*RAW*) Konflikt
    - Gegenabhängigkeit $->$ Eine Instruktion liest ein Register, das anschließend beschrieben wird
    - Ausgabeabhängigkeit $->$ Eine Instr. überschreibt das Ergebnis einer anderen
  / Steuerflusskonflikt: Durch Sprungbefehle hervorgerufen
]
