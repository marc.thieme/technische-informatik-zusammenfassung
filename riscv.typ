#import "lib.typ": *

= RISC-V
== Adressierungsformate:
+ Immediate Adressing
+ Register adressing
+ Register indirect adressing with displacement 
  (Speicheradresse ist die Summe des Registerinhalts und Konstante)
+ Program Counter Relative Adressing

== Befehlsformate
/ opcode: 7 Bit Opcode des Befehls
/ rd: 5 Bit Registernummer der Zielregisters
/ rs1: Registernummer der Basisadresse
/ rs2: Registernummer des Quelloperanden
/ imm[11\:5]imm[4\:0]: Byte-Offset kann ein Wort in einem Bereich von $plus.minus 2^11$
  Bytes bezüglich der Basisadresse referenzieren.\
  Aufgeteilt in zwei Felder, damit die die rs1- und rs2-Felder in jedem
  Format immer an denselben Bitstellen sind
/ func3: 3 Bit Funktionscode
/ func7: 7 Bit Funktionscode
/ Immediate: 12 Bits Konstante

#separate()

#image("res/riscv-formats.png")

#separate()

*add x9, x20, x21*
#image("res/r-format.png")

*addi x1, x2, 1000*
#image("res/l-format.png")

*sw x1, 1000(x2)*
#image("res/s-format.png")

*lui x10, 0x87654*
*addi x10, x10, 0x321*
#image("res/u-format.png")

*beq x4, x5, End*
#image("res/sb-format.png")

*jal x1, 2000*
#image("res/uj-format.png")

#separate()

#image("res/r-type-table.png")
#image("res/l-type-table.png")
#image("res/s-type-table.png")
#image("res/sb-type-table.png")
#image("res/jump-and-link-uj-format.png")

#link("res/links/riscv-card.pdf")[LINK TO CARD]

#let s(s) = s.split(",").map(str.trim)

== Instructions to remember:
#table(
  columns: 7,
  table.header([Inst], [Name], [FMT], [Opcode], [funct3], [funct7], [Description]),
[addi],[ADD Immediate],          [I],[00100011],[0x0],[],              [rd=rs1+imm],
[slli],[Shift Left Logical Imm], [I],[0010011], [0x5],[imm[5:11]=0x00],[rd = rs1 << imm[0:4]],
[slti],[Set less than immediate],[I],[0010011], [0x2],[imm[5:11]=0x00],[rd = (rs1 < imm)?1:0],
[lw],  [Load Word],              [I],[0000011], [0x2],[],              [rd = M[rs1+imm][0:31]],
[sb],  [Store Byte],             [S],[0100011], [0x0],[],              [M[rs1+imm][0:7] = rs2[0:7h]],
[add], [ADD],                    [R],[0110011], [0x0],[0x00],          [rd = rs1 + rs2],
[lui], [Load Upper Imm],         [U],[0110111], [],   [],              [rd = imm << 12],
[beq], [Branch==],               [B],[1100011], [0x0],[],              [if(rsa1 == rs2) PC += imm],
)

== Register usage
#image("res/register-usage.png", height: 40%)

== Processor Implementation
#image("res/ riscv-processor-structure-and-pipeline-steps.png")
