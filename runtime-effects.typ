#import "lib.typ": *

= Laufzeiteffekte
== Modellierung
#definition[Automat][
  $(E, A, Z, delta, omega, z_0)$, wobei:
  - $E$ Eingangsbelegungen (Eingabealphabet)
  - $A$ Ausgangsbelegungen (Ausgangsalphabet)
  - $Z$ Zusatndsmenge
  - $delta: Z times E -> Z$: Zustandsüberführungsfunktion
  - $omega: Z times E -> A$: Ausgabefunktion
]
#definition[Moore/Mealy-Automat][
/ Moore Automat: _Nur_ Zustand geht in Ausgabefunktion mit ein\
  $->$ Synchrone und Asynchrone Schaltwerke
/ Mealy Automat: _Auch_ Eingabebelegung geht in Berechnung des Ausgabewertes mit ein
  1. Art: Zuerst wird neue Ausgabebelegung aus anliegender Eingabebelegung gebildet. 
    Dann wird in neuen Folgezustand gewechselt\
    $->$ Synchrone Schaltwerke
  2. Art: Zuerst wird Folgezustand berechnet und dann mit derselben Eingabebelegung die Ausgabebelegung gebildet.
    $->$: Asynchrone Schaltwerke
]

== Umsetzung
#term[Synchrones/Asynchrones Schaltwerk][
  / Synchrones Schaltwerk: Zustandsspeicher werden von (mind.) einem Takt T gesteuert
  / Asynchrones Schaltwerk: Sonst
]

#term[Pegelsteuerung][
  / Pegelgesteuert (Latches): Nur wenn Takt wert 1 hat, wird Zustand geändert. Hat Takt 0, wird Zustand gespeichert.
  / Flankengestuert: Speicher ändert sich bei positiver oder negativer taktflanke.
]

#figure(caption: [Flussmatrix mit stabilen Zuständen], image("res/flussmatrix.png", height: 15%))

#term[Stabiler Zustand][
  Ein Zustand ist unter einer Eingabebelegung stabil, wenn er in sich selbst übergeht.
]

#note[][
  Bei asynchronen Schaltwerken kommt der Wahl einer geeigneten Zustandskodierung 
  entscheidende Bedeutung zu!
]

=== Synchron
#term[Wettlauf][
  _Szenario_: In der Kodierung von einem Zustand zu seinem Folgezustand ändern sich 2 Bits.

  Diese Bits ändern sich, je nach Verzögerungszeiten nicht genau Gleichzeitig.
  Darum werden verschiedene Pfade eingeschlagen, die erstmal beide nicht der gewollte Zustand sind.

  Das nennt man einen *Wettlauf*. Bei einem *kritischen Wettlauf* wird so ein falscher stabiler Zustand erreicht.

  Wir suchen eine *Wettlauffreie* Zustandskodierung.
]

#image("res/automatentabelle.png")

#definition[Erregungsmatrix][
  Die Erregungsmatrix erhält man, indem man in die Flussmatrix die Zustandskodierung einsetzt.

  $->$ aus Ihr kann die Funktionstabelle gewonnen werden.
]

#approach[Analyse von asynchronen Schaltwerken][
+ Zuerst die Funktionstabelle aufzeichnen
+ Druch umordnen kann die Erregungsmatrix gewonnen werden
]

#property[Automat der Analyse][
  Da die Analyse immer einen eigenen $q$-Ausgang berücksichtigt bringt sie immer einen Mealy-Automaten hervor.
]

== Flip Flops als Zustandsspeicher und Taktgeber
#definition[Flip Flops][
  + RS-FlipFlop (Synchronous): 2 Eingänge $s, r$ um jeweils 1 oder 0 zu setzen. 
    2 Ausgänge, um Zustand und Inverse auszulesen.
    Es muss immer die Nebenbedingung $s and r = 0$ eingehalten werden.
  + RS-FlipFlop (Asynchronous): Zusätzlich einen Takt Input, der mit den Inputs verundet wird.
  + D-Latch (Synchron): RS-Latch, wo ein Signal einmal negiert und einmal normal angelegt wird.
    $->$ $s == 1 => z ~> 1, quad s == 0 => z ~> 0$. Das garantiert auch, dass die Nebenbedingung der RS-Latch erfüllt ist.
  + D-Latch (Asynchron, flankengesteuert): Indem man 2 pegelgesteurte D-Latches hintereinanderschaltet.
  + JK-FlipFlop: RS-FlipFlop, sodass $r and s = 0$ den Zustand invertiert.
  + T-FlipFlop: Wenn bei Taktsignal 1 anliegt, wird der Speicherzustand invertiert.
]

#note[Notation Taktsignalen][
  - Ein Bauteilinput S#text(red)[1] ist von einem Taktinput C#text(red)[1] abhängig
  - Ein Flankegesteuerter Signalinput wird über ein Dreieck dargestellt. 
    Reagiert es bei einer negativen Flanke wird ein Kreis davorgezeichnet.
]

== Synchron
#todo[Synchrone Schaltungen Design][]

== Spezielle Schaltwerkbausteine
/ Register: Mehrere D-FlipFlops (1 pro Bit) parallelgeschaltet
/ Schieberegister: Mehrere D-FlipFlops oder Register in Reihe geschaltet
