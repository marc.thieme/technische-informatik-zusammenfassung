#import "lib.typ": *

== Mima
#image("res/mima-diagram.png", width: 48%)

#image("res/mima-microinstructions.png", width: 48%)

== Mikroprogrammierung Phasen
+ Fetch Phase
+ Decode Phase
+ Execute Phase

== Adressierungsarten
/ Direktoperand (immetiate *i-*): Der Operand steht als Konstante im Befehl.
/ Direktadressierung: Speicheradresse des Operands steht im Befehl.
/ Indirekte Adressierung: Die Adresse im Befehl zeigt auf das Wort im Speicher, dass die Adresse des Operanden enthält.
/ Register Adressierung: Der Operand steht im Register, dessen Adresse im Befehl steht.
/ Implicit Registeradressing: Registeradresse steht im Opcode
/ Registeradressierung mit Increment: 
