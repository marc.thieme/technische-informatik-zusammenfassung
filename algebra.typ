#import "lib.typ": *

= Algebra
== Axiome
#theorem[Elementare Boolsche Regeln][
  / Absorptionsgesetz: $x or (x and y) &= x quad x and (x or y) &= x$ 
  / Idempotenzgesetz: $x and x &= x         quad x or x &= x$
  / Komplementregeln: $x and neg(x) &= 0    quad x or neg(x) &= 1$
  / Netrale Elemente: $x or 0 &= x          quad x and 1 &= x$
]

#theorem[Weitere Regeln][
  / Distributivgesetz: $a b or c = (a or c) (b or c) quad (a or b) c = a c or b c$
  / De Morgansche Regeln: $neg(a b) = neg(a) or neg(b) quad neg(a or b) = neg(a) neg(b)$
  Die De Morganschen Regeln lassen sich erweitern:
  $neg(a b c) = neg(a) or neg(b) or neg(c)$
]

== Operatorensysteme
#theorem[$(and, or, not)$][ist ein vollständiges Operatorensystem][]

== Normalformen
#term[Implikat/Implikant][
  / Implikant: $K$ ist Implikant von $y$, wenn $K -> y$
  / Implikat: $K$ ist Implikat von $y$, wenn $neg(K) -> neg(y)$
]

#definition[Konj./Disj. Normalformen][]

#definition[Minterm & Maxterm][
  / Minterm: Implikat einer Funktion $y$, sodass jede Variable genau einmal vorkommt
  / Maxterm: Implika#strong[n]t einer Funktion $y$, sodass jede Variable genau einmal vorkommt
]

#term[
  / Minterm: Ein Term, der für alle bis auf eine Belegung 0 ergibt.
  / Maxterm: Ein Term, der für alle bis auf eine Belegung 1 ergibt.
]

#theorem[Shannon Entwicklungssatz][$
  f(x_1, ..., x_n) = [x_i and f(..., x_(i-1), 1, x_(i+1), ...)] or [neg(x_i) or f(..., x_(i-1), 0, x_(i+1), ...)]
$]

=== KV-Diagram
#definition[Primimplikant][
  / Primimplikant $p$: $forall_("implikant" q != p) not(p -> q)$
]

#theorem[][
  Jede Funktion ist als Implikant ihrer Primimplikanten darstellbar.
]

=== Konsensus
#theorem[Consensus Theorem][
  $ y x or neg(x) z  or y z = x y or neg(x) z$
]

=== Verfahren
#image("res/vorgehensweise-minimieren.png")

=== Überdeckungsfunktion
#image("res/ueberdeckungsfunktion.png")

