#import "lib.typ" as lib: *

= Cache
#term[Cache cahracteristics][
  / Hit Time: Zugriffszeit bei Treffer: Ziet für Zugriff + Zeit um festzusetellne, ob Treffer oder nicht
  / Miss Penalty: Zeit, um Block von unterer Ebene in oberee Ebene zu laden
]

#term[Temporal and Spatial Locality][
  Tendenz des Prozessors auf Objekte zuzugreifen,
  - auf die er gerade erst zugegriffen hat
  - wo er auf benachbarte Objekte eben zugegriffen hatte
]

/ Tag: Der Tag einer Cache Zeile identifiziert den Slot im Cache, indem die Zeile abgespeichert wird

#theorem[Ein N-Way Set Associative Cache benötigt n vergleicher]

== Cache Anbindungen
=== Look Through Cache
#image("res/look-through-cache.png")

=== Look Aside Cache
#image("res/look-aside-cache.png")

=== Backside Cache
#image("res/backside-cache.png")

== Cache Arten
#definition[Cache Arten][
/ Direct Mapped Cache: Jeder Block ist zu einer eindeutigen Cache Zeile zugeordnet
/ Fully Associative Cache: Jeder Block kann auf jede Cache Zeile abgebildet werden
/ n-way Set Accociative Cache: Index Field (Direct Mapped) und Tag Feld (Associative)
]

#definition[Ersetzungsstrategien][
  + LRU
  + Pseudo-LRU
  + Random
  + FIFO
  + Belady's Algorithm (theoretisch)
]

