#import "lib.typ" as lib: *

#show: project.with("Technische Informatik", "Henkel & Karl", "Marc Thieme")
#show: lib.break-page-after-chapters()
#show "mplikant": it => [implika#strong[n]t]
#show image: set align(center)


= Digitaltechnik
#include "computing-arithmetic.typ"
#include "algebra.typ"
#include "circuits.typ"
#include "runtime-effects.typ"
#include "exam.typ"

= Rechnerorganisation
#include "mima.typ"
#include "riscv.typ"
#include "pipelining.typ"
#include "cache.typ"
#include "memory-management.typ"
