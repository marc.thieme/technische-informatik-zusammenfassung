#import "lib.typ": *

= Memory Management
#term[
  / Access time: Die maximale Dauer, die vom Anlegen einer Adresse an den Speicher bis zur Ausgabe vergeht
  / Zykluszeit: Die minimale Dauer, die zwischen zwei hintereinander folgenden Aufschaltungen 
    von Adressen an den Speicher vergehen muss
]

#definition[Burst mode][
  Mit jedem folgenden Takt werden darauffolgende Elemente zu dem angeforderten mitausgegeben.
]

#approach[
  First send Row (RAS) and then (CAS)
]
#image("res/ram-signals.png")

== Cache
#definition[Cache Adressierungsarten][
  - virtueller Cache
  - realer Cache
  - virteull/realer Cache
]

#definition[Cache Kohäraenzarten][
  / Write Through Cache: 
]


