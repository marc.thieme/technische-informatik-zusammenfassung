#import "@local/theorems:0.0.5": *
#import "@local/lecture-notes-template:0.0.4": *

#let (
  definition,
  task,
  theorem,
  term,
  property,
  approach,
  note,
  todo
) = init-theorems(
  "theorems",
  definition: ("Definition",),
  task: ("Aufgabe", ),
  theorem: ("Satz",),
  term: ("Terminologie",),
  property: ("Eigenschaft",),
  approach: ("Ansatz",),
  note: ("Beachte",),
  todo: ("TODO",)
)

#let neg(x) = $overline(x)$
