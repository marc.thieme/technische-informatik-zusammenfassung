#import "lib.typ": *

= Schaltungen
== Physics
#definition[Transistorenarten][
  / npn-Bipolartransistor: Normaler Transistor
  / MOSFET: Ohne Stromfluss
  / nMOS: Selbstsperrend, Ladungsträger sind Elektronen
  / pMOS: Selbstsperrend, "Kanal wird mit positiven Ladungsträgern angereichert"
]
#property[Leitcharakteristik][
  - *n*: Leitet unter positiver Spannung
  - *p*: Leitet unter negativer Spannung
]
#term[
  Der Pfeil zeigt auf die Platte $-->$ n-Kanal.
][]
#term[Notation][
  - Selbstsperrend: Gestrichtelter Stricht
  - Selbstleitend: Durchgezogener Strich
]
#[
#set image(height: 40%)
#show image: align.with(center)
#image("res/mosfets.png")
#image("res/current-curve-mosfet.png")
*Transmission Gate*
#image("res/transmission-gate.png")
=== Inverter
#image("res/cmos-inverter.png")
=== NAND
#image("res/cmos-nand.png")
=== NOR
#image("res/cmos-nor.png")
=== Generell
#image("res/cmos-principle.png")

#link("res/links/Kapitel_3_SS24.pdf#page=182")[*LINK TO SLIDES*]
]
#theorem[CMOS Construction][
  Eine Reihenschaltung in dem einen Netz entspricht einer Parallelschaltung in dem jeweils anderen Netz.
]


== CMOS
#term[CMOS][
  - Complementary Metal Oxide Semiconductor
  - Funktionswerte werden als Spannungspegel repräsentiert
  - Es fließt kein Strom im statischen Zustand der Eingangsvariablen
]

=== Inverter
#image("res/cmos-inverter.png")

== Building Blocks
#definition[Schaltnetz/Schaltwerk][
  / Schlatnetz: Eine Schaltungsystem, das *Rückkopplungsfrei* ist.
  / Schaltwerk: Ein Schaltsystem mit Rückkopplung, i.e. Zustand.
]

Ein Multiplexer kann anhand des $n$-wertigen Steuersignals eines aus $2^n$ Eingabesignalen als Ausgabe schalten.
#definition[Multiplexer][
  Baustein mit mehreren Eingängen und einem Ausgang
  - über n Steuerleitungen kann
  - einer der $2^n$ Eingänge auf den Ausgang geschaltet werden.
]
#image("res/multiplexer.png")

#separate()
#image("res/multiplexer-recursion.png")

Implementieren einer Funktion mit Multiplexern
#image("res/multiplexer-implementation-1.png")
#separate()
#image("res/multiplexer-implementation.png")
#separate()
#image("res/multiplexer-implementation-2.png")
#separate()
#align(center, image("res/decoder-implementation.png", height: 20%))

#definition[Demultiplexer][
  Schaltet ein Eingabesignal anhand eines Steuersignals auf eines von $2^n$ Ausgabesignalen.
]

==
#definition[Totzeitmodell][
  Jedes echte Gatter wird modelliert als ein ideales Gatter + ein reines Verzögerungsgatter.
]
#definition[Verzögerungsteil/Verzweigungsteil][]
#image("res/delay-branch-parts.png")
#definition[Strukturausdruck][
  Die algebraische Darstellungsform eines Verzweigungsteils
  - Jede Variable darf nur einmal vorkommen und muss anschließend neu indiziert werden
    (da jedes Vorkommen einer Variablen und anderen Verzögerungsumständen stattfindet)
]

#term[Eingabewechsel/Übergang][
  / Eingabewechsel: Die Änderung einer oder mehrerer Eingangsvariablen
    zu einem bestimmten Zeitpunkt
  / Übergang: Der Vorgang im Schaltnetz, der nach einem Eingabewechsel ausgelöst wird
]

#term[Hazard Fehler][
  `Ein *Hazardfehler* ist eine mehrmalige Änderung der Ausgangsvariablen während eines Übergangs `

  `Ein *Hazard* ist die durch das Schlatnetz gegebene logisch-strukturelle Vorbedingung für einen Hazardfehler, 
  ohne Berücksichtigung der konkreten Verzögerungswerte`

  Eine fehlerhafte, momentane falsche Ausgabe, die Aufgrund von verschiedenen Verzögerungen im System stattfindet.

]

#theorem[
  Jeder Übergang kann einem Eingabewechsel zugeordnet werden.
]

#property[Für einen Hazardfehler interessieren konkrete Verzögerungszeiten nicht][]

#theorem[Satz von Eichelberger][
  Die Disjunktion aller Primimplikanten ist frei von 
+ statischen Strukturhazards
+ allen dynamischen Strukturhazards, falls nur eine Eingabevariable wechselt
]

== Speicherbasierte Implementierung
#image
#note[][
- Es werden effizient mehrere Funktionen implementiert.
- Zur Platzeffizienten Implementierung kann die Disjunktive Minimalform herangezogen werden, da so die Größe der Arrays minimiert werden kann.
- Durch Bündeloptimierung lassen sich außerdem Ausgänge des UND-Arrays in Form der Primimplikanten teilen.
]

== Übergänge
#term[Dynamischer/Statischer Übergang][
  / Dynamischer $b$-Übergang: Anfangswert des Ausgangssignals ist $b$, Endwert auch
  / Statischer $b p$-Übergang: Anfangswert des Ausgangssignals ist $b$, Endwert ist $p$
]

#term[Funktions-/Strukturhazard][
  / Funktionshazard: die Ursache des Hazards liegt in der zu realisierenden Funktion selbst
  / Strukturhazard: Die Ursache liegt in der Umsetzung/Struktur des Schaltnetzes
]

#property[
  Ein Funktionshazard tritt in jedem Schaltnetz der zu realisierenden Funktion auf.
  Der Hazardfehler kqann durch günstige Wahl der Verzögerungswerte behoben werden, der Hazard selbst allerdings nicht
]

#theorem[Funktionshazardbehaftheit][
  Wir betrachten einen Übergang als Zellen im KV-Diagramm zwischen Anfangsbelegung und Endbelegung des Übergangs.

  Ein Übergang ist funktionshazardbehaftet, gdw. es gibt einen Weg, für den die Folge der zugehörigen Funktionswerte 
  (0 oder 1 im KV-Diagramm) nicht monoton ist, dh. wo mindestens 2 mal der Funktionswert wechselt.
]

#theorem[Strukturhazardbehaftheit][
  Man betrachtet den Strukturausdruck.

  Man betrachtet das KV-Diagramm dessen.

  Für einen Übergang, d.h. alle Pfade zwischen der Anfangs- und Endeingabe für diesen Übergang 
  muss jeder Pfad monoton sein.
]

#approach[Behebung von Strukturhazards][
  - 1-Strukturhazard: Realisiere mit einem UND-Glied einen Primimplikanten,
    - der Anfangs- und Endbelegung des Übergangs enthält
    - der mit dem UND-Ausgang disjunktiv verknüpft wird
  - 0-Strukturhazard: Realisiere mit einem ODER-Glied einen Primimplikaten,
    - der Anfangs- und Endbelegung des Übergangs enthält
    - der mit dem ODER-Ausgang konjunktiv verknüpft wird
]
